
class ListNode:
    def __init__(self, data, next_node=None):
        self.data = data
        self.next = next_node

    def __repr__(self):
        return str(self.data)

    def to_array(self):
        head = self
        result = []
        while head is not None:
            result.append(head.data)
            head = head.next
        return result

def overlapping_no_cycle_lists(L1, L2):
    pass


## Test case 1 ##############################
a, b, c, d, e, f, g, h = ListNode(1), ListNode(2), ListNode(3), ListNode(4), ListNode(5), ListNode(6), ListNode(7), ListNode(8)
con = ListNode(10)

a.next, b.next, c.next = b, c, con
d.next, e.next = e, con
con.next, f.next, g.next = f, g, h

"""
a-b-c-|
      |
 d-e-con-f-g-h
"""
print(a.to_array())
print(d.to_array())
print(overlapping_no_cycle_lists(a, d))   # -> 10
