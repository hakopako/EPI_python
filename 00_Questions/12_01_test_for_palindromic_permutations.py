# time complexity: O(n) -> length of the staring
# space complexity: O(c) -> number of distinct characters appering in the string.

def can_form_palindrome(s):
    pass

## Test case

test_case = [
    'abcdedcba',   # -> True
    'aaabbbbaaa',  # -> True
    'xscxsc',   # -> True
    'xscx',   # -> False
    'aaaabc',  # -> False
    'a',  # -> True
    'ab',  # -> False
]

for t in test_case:
    r = can_form_palindrome(t)
    print(r)
