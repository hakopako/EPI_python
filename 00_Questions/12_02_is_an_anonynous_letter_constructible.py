
def is_letter_constructible_from_magazine(letter_text, magazine_text):
    pass

## Test case

test_case = [
    ['abcd', 'aabbccddssww'],   # -> True
    ['abcdz', 'aabbccddssww'],  # -> False
    ['abcdzz', 'aabbccddsswwz']  # -> False
]

for t in test_case:
    r = is_letter_constructible_from_magazine(t[0], t[1])
    print(r)
