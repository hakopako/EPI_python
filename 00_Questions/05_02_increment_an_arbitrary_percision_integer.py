

def plus_one(A):
    pass

### Test case
test_case = [
    [1,2,9], # -> [1, 3, 0]
    [9,9,9], # -> [1, 0, 0, 0]
    [1,2,3], # -> [1, 2, 4]
    [0], # -> [1]
]
for t in test_case:
    plus_one(t)
    print(t)
