# time complexity: O(n)
# space complexity: O(1)


def is_palindrom(s):
    pass

### Test case
test_case = [
    'Able was I, ere I saw Elba!',  # -> True
    ',,,,b,,,,,,',  # -> True
    ',,,b,,,x,,,',  # -> False
    '',  # -> True
    'abcba',  # -> True
    'zxdcbn',  # -> False
    'abba',  # -> True
]

for t in test_case:
    r = is_palindrom(t)
    print(r)
