
def reverse_words(s):
    pass


### Test case
test_case = [
    'Alice likes Bob',   # -> Bob likes Alice
    'a b c d e',   # -> e d c b a
]

for t in test_case:
    reverse_words(t)
    print(t)
