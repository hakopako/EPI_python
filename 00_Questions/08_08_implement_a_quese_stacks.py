
class Queue:

    def __init__(self):
        self._enq, self._deq = [], []

    def enqueue(self, x):
        pass

    def dequeue(self):
        pass


### Test case
q = Queue()

q.dequeue()  # warning
q.enqueue(3)
q.enqueue(5)
q.enqueue(4)
print(q.dequeue())  # -> 3
q.enqueue(8)
print(q.dequeue())  # -> 5
print(q.dequeue())  # -> 4
