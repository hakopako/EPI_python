# time complexity: O(n)
# space complexity: O(1) ?????

def is_well_formed(s):
    pass


### Test case
test_cases = [
    '()',  # True
    '[(){}]',   # True
    '[{}{}{{{{{((()))(((()((((())))))}}}}}]',  # False
    '][()]',  # False
    '[[[[[[[[[[[[[[[[[[[[]]]]]]]]]]]]]]]]]]]]',  # True
    '[]]',   # False
]

for t in test_cases:
    print(is_well_formed(t))
