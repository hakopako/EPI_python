def encoding(s):
    pass

def decoding(s):
    pass


### Test case
test_case_1 = [
    'aaaabcccaa',  # -> 4a1b3c2a
    'aaaaaaaaaaaaaaab',  # -> 15a1b
    '',  # -> ''
    'abcdefg',  # -> 1a1b1c1d1e1f1g
]

test_case_2 = [
    '4a1b3c2a',  # -> aaaabcccaa
    '15a1b',  # -> aaaaaaaaaaaaaaab
    '', # -> ''
    '1a1b1c1d1e1f1g',  # -> abcdefg
]

for t in test_case_1:
    #r = my_encoding(t)
    r = encoding(t)
    print(r)

for t in test_case_2:
    #r = my_decoding(t)
    r = decoding(t)
    print(r)
