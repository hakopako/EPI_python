
class ListNode:
    def __init__(self, data, next_node=None):
        self.data = data
        self.next = next_node

    def __repr__(self):
        return str(self.data)

    def to_array(self):
        head = self
        result = []
        while head is not None:
            print(head.data)
            result.append(head.data)
            head = head.next
        return result

def has_cycle(head):
    pass



## Test case 1 ##############################
a, b, c, d, e, f, g, h = ListNode(1), ListNode(2), ListNode(3), ListNode(4), ListNode(5), ListNode(6), ListNode(7), ListNode(8)
con = ListNode(10, d)

a.next, b.next, c.next = b, c, con
d.next, e.next, f.next, g.next = e, f, g, con

print(has_cycle(a))   # -> 10


## Test case 2 ##############################
a2, b2, c2, d2, e2, f2, g2, h2 = ListNode(1), ListNode(2), ListNode(3), ListNode(4), ListNode(5), ListNode(6), ListNode(7), ListNode(8)
a.next, b.next, c.next, d.next, e.next, f.next, g.next = b2, c2, d2, e2, f2, g2, h2

print(has_cycle(a))   # -> None
