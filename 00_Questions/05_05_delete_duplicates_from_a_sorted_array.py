# time complexity O(n)
# space complexity O(1)

# Return the number of valid entries after deletion.
def delete_duplicates(A):
    pass

### Test case
test_case = [
    [2,3,5,5,7,11,11,13],  # -> [2, 3, 5, 7, 11, 13]
    [0], # -> [0]
    [],  # -> []
    [2,2,2,2,2,2,2,2],  # -> [2]
]
for t in test_case:
    r = delete_duplicates(t)
    print(t, t[:r], sep=" -> ")
