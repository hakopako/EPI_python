# time complexity: O(logn) -> Each iteration reduces the size of the candidate set by half
# space complexity: O(1)


def search_first_of_k(A, k):
    pass


### Test case

test_case = [
    [[-14, -10, 2, 108, 108, 243, 285, 285, 285, 401], 108],   # -> 3
    [[-14, -10, 2, 108, 108, 243, 285, 285, 285, 401], 285],   # -> 6
    [[-14, -10, 2, 108, 108, 243, 285, 285, 285, 401], 0],   # -> -1
]

for t in test_case:
    r = search_first_of_k(t[0], t[1])
    print(r)
