# time complexity O(1)
# space complexity O(1)


class ListNode:
    def __init__(self, data, next_node=None):
        self.data = data
        self.next = next_node

    def __repr__(self):
        return str(self.data)

    def to_array(self):
        head = self
        result = []
        while head is not None:
            result.append(head.data)
            head = head.next
        return result

# Assume node_to_delete is not tail.
def delete_from_list(node_to_delete):
    pass


### Test case
a, b, c, d, e, f, g, h = ListNode(1), ListNode(2), ListNode(3), ListNode(4), ListNode(5), ListNode(6), ListNode(7), ListNode(8)
a.next, b.next, c.next, d.next, e.next, f.next, g.next = b, c, d, e, f, g, h

print(a.to_array())
delete_from_list(c)
print(a.to_array())  # -> [1, 2, 4, 5, 6, 7, 8]
