# time complexity is O(log(N));
# space complexity is O(1).

def solution(N):
    pass

## Test case
test_case = [
    9,    # 1001  -> 2
    529,  # 1000010001 -> 4
    20,   # 10100 -> 1
    15,   # 1111 -> 0
    1041, # 10000010001 -> 5
]

for t in test_case:
    print(solution(t))
