# expected worst-case time complexity is O(N)
# expected worst-case space complexity is O(X)

def solution(X, A):
    pass

# Test case
test_case = [
    [5, [1,3,1,4,2,3,5,4]],    # -> 6
    [4, [1,4,1,2,1,3,2,3,1]],  # -> 5
    [6, [1,3,1,4,2,3,5,4]],    # -> -1
]

for t in test_case:
    print(solution(t[0], t[1]))
