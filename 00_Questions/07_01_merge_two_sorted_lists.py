# time complexity: O(n + m)
# space complexity: O(1)

class ListNode:
    def __init__(self, data=0, next_node=None):
        self.data = data
        self.next = next_node

    def to_array(self):
        node = self
        result = []
        while node is not None:
            result.append(node.data)
            node = node.next
        return result

def merge_two_sorted_lists(L1, L2):
    pass


### Test case
test_case = [
    [ListNode(2, ListNode(5, ListNode(7))), ListNode(3, ListNode(11))],  # -> 2, 3, 5, 7, 11
]


for t in test_case:
    print(t[0].to_array(), t[1].to_array())
    r = merge_two_sorted_lists(t[0], t[1])
    print(r.to_array())
