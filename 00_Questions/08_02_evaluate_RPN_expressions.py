# time complexity: O(n)
# space complexity: O(1) ?????

def evaluate(RPN_expressions):
    pass


### Test case

test_cases = [
    '2,3,*',   # -> 6
    '3,4,+,2,*,1,+',  # -> 15
    '1,1,+,-2,*',    # -> -4
    '-641,6,/,28,/',  # -> -2991.333333333333
]

for t in test_cases:
    print(evaluate(t))
