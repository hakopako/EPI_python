# time complexity: O(n)
# space complexity: O(n) ?????

def shortest_equivalent_path(path):
    pass

### Test case

test_cases = [
    'sc//./../tc/awk/././',   # -> tc/awk
    './aaa/bbb/.././ddd/ccc.txt',    # -> ./aaa/ddd/ccc.txt
    '/aaa/../cc/./ddd/../../././eee/ff/./ggg.tsv',    # -> /eee/ff/ggg.tsv
    './aaa/../../bb',  # -> ./../bb
    '/aa/../../cc'   # -> path error
]

for t in test_cases:
    #r = my_solution(t)
    r = shortest_equivalent_path(t)
    print(r)
