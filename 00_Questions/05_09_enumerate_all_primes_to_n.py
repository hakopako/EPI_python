# time complexity O(n**3/2) ???
# space complexity O(n)

def generate_primes(n):
    pass

### Test case
test_case = [
    18,   # -> [2, 3, 5, 7, 11, 13, 17]
    2,    # -> [2]
    3,    # -> [2, 3]
    9     # -> [2, 3, 5, 7]
]

for t in test_case:
    r = generate_primes(t)
    print(r)
