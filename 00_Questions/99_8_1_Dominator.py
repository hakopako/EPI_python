"""
A zero-indexed array A consisting of N integers is given.
The dominator of array A is the value that occurs in more than half of the elements of A.
"""
# expected worst-case time complexity is O(N);
# expected worst-case space complexity is O(1),

def solution(A):
    pass

## Test case
test_case = [
    [3,4,3,2,3,-1,3,3],   # -> 3
    [-1,2,3,1,2,1,1,5,1,1,4],   # -> 1
]

for t in test_case:
    print(solution(t))
