"""
check if input array consists 1 to N numbers.
"""

def solution(A):
    pass

## Test case
test_case = [
    [4, 1, 3, 2],   # -> 1  : ture
    [4, 1, 3],      # -> 0  : false
]

for t in test_case:
    print(solution(t))
