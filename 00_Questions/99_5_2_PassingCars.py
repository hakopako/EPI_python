"""
0 represents a car traveling east,
1 represents a car traveling west.
"""

def solution(A):
    pass

## Test case
test_case = [
    [0,1,0,1,1],   #(0, 1), (0, 3), (0, 4), (2, 3), (2, 4) -> 5
    [0,0,0,0,0,0,1,1],   # -> 12
    [1,1,0,0,0],   # -> 0
]

for t in test_case:
    print(solution(t))
