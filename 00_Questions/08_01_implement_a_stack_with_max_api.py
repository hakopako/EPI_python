# time complexity: O(1)
# space complexity: O(n)

class Stack:
    
    def __int__(self):
        pass

    def empty(self):
        pass

    def max(self):
        pass

    def pop(self):
        pass

    def push(self, x):
        pass


### Test case

stack = Stack()
stack.pop()  # warning
stack.push(3)
stack.push(5)
stack.push(4)
print(stack.max())  # 5
stack.push(8)
print(stack.max())  # 8
stack.pop()
print(stack.max())  # 5
