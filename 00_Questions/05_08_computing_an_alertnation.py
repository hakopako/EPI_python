
# SKIP
# A[0] < A[1] > A[2] < .....
def rearrange(A):
    for i in range(len(A)):
        A[i:i + 2] = sorted(A[i:i + 2], reverse=i % 2)  # TODO: what's this ??????


### Test case
test_case = [
    [12,11,13,9,12,8,14,13,15],   # -> [11, 13, 9, 12, 8, 14, 12, 15, 13]
]

for t in test_case:
    rearrange(t)
    print(t)
