
# SKIP
# not use type converter
def int_to_string(x):
    is_negative = False
    if x < 0:
        x, is_negative = -x, True

    s = []
    while True:
        s.append(chr(ord('0') + x % 10))
        x //= 10
        if x == 0:
            break

    return ('-' if is_negative else '') + ''.join(reversed(s))

def string_to_int(s):
    return functools.reduce(
        lambda running_sum, c: running_sum * 10 + string.digit.index(c),
        s[s[0] == '-':], 0) * (-1 if s[0] == '-' else 1)


### Test case
test_case_x = [
    123,
    -123,
    120,
    -120,
    0,
]

test_case_s = [
    '123',
    '-123',
    '120',
    '-120',
    '0',
]
for t in test_case_x:
    print(int_to_string(t))

for t in test_case_s:
    print(string_to_int(t))
