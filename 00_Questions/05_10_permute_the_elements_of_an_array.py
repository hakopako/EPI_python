
def apply_permutation(perm, A): #TODO: it's tough for me to write this function from scratch.....
    pass

### Test case
test_case = [
    [[3,1,2,0], ["a", "b", "c", "d"]], # -> ['d', 'b', 'c', 'a']
    [[2,5,0,1,3,4], ["a", "b", "c", "d", "e", "f"]],  # -> ['c', 'd', 'a', 'e', 'f', 'b']
]
for t in test_case:
    apply_permutation(t[0], t[1])
    print(t[1])
