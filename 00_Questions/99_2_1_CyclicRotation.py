
def solution(A, K):
    pass

## Test case
test_case = [
    [[3, 8, 9, 7, 6], 3],    # -> [9, 7, 6, 3, 8]
    [[0, 0, 0], 1],     # -> [0, 0, 0]
    [[1, 2, 3, 4], 4],  # -> [1, 2, 3, 4]
]

for t in test_case:
    print(solution(t[0], t[1]))
