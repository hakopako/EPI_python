# time complexity: O(n)
# space complexity: O(1)

# a -> dd, b -> ""
## under specifit condition: s has extra space for converted string.
def replace_and_remove(size, s):
    pass


### Test case
test_case = [
    ['a', 'c', 'd', 'b', 'b', 'c', 'a'],  # -> ['d', 'd', 'c', 'd', 'c', 'd', 'd']
    ['a', 'a', 'a'],   # -> ['d', 'd', 'd', 'd', 'd']
    ['b', 'b', 'b'],   # -> []
    []   # -> []
]

for t in test_case:
    r = replace_and_remove(len(t), t)
    print(t[:r])
