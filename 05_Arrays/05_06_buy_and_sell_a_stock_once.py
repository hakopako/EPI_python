
def buy_and_sell_a_stock_once(prices):
    min_price_so_far, max_profit = float('inf'), 0.0
    for price in prices:
        min_price_so_far = min(min_price_so_far, price)
        max_profit_sell_today = price - min_price_so_far
        max_profit = max(max_profit, max_profit_sell_today)
    return max_profit

### Test case
test_case = [
    [310,315,275,295,260,270,290,230,255,250],    # -> 30
    [0],  # -> 0
    [],   # -> 0
]
for t in test_case:
    r = buy_and_sell_a_stock_once(t)
    print(r)
