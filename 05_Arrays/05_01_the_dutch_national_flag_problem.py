# time complexity O(n)
# space complexity O(1)

def dutch_flag_partition(pivot_index, A):
    pivot = A[pivot_index]
    smaller = 0
    for i in range(len(A)):
        if A[i] < pivot:
            A[i], A[smaller] = A[smaller], A[i]  # memo: Swap
            smaller += 1

    larger = len(A) - 1
    for i in reversed(range(len(A))):
        if A[i] < pivot:
            break
        elif A[i] > pivot:
            A[i], A[larger] = A[larger], A[i]
            larger -= 1

### Test case
test_case = [
    [4, [2,6,3,8,5,2,5,9,6,4]], # -> [2, 3, 2, 4, 5, 5, 6, 9, 6, 8]
    [3, [-1,4,-5,-2,-6,-5,-2,0]], # -> [-5, -6, -5, -2, -2, 4, -1, 0]
    [0, [3]], # -> [3]
    [3, [2,2,2,2,2,2]], # -> [2, 2, 2, 2, 2, 2]
]
for t in test_case:
    dutch_flag_partition(t[0], t[1])
    print(t[1])
