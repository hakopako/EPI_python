
def apply_permutation(perm, A): #TODO: it's tough for me to write this function from scratch.....
    for i in range(len(A)):
        next = i
        while perm[next] >= 0:
            A[i], A[next] = A[next], A[i]
            temp = perm[next]
            perm[next] -= len(perm)
            next = temp
    perm = [a + len(perm) for a in perm]

### Test case
test_case = [
    [[3,1,2,0], ["a", "b", "c", "d"]], # -> ['d', 'b', 'c', 'a']
]
for t in test_case:
    apply_permutation(t[0], t[1])
    print(t[1])
