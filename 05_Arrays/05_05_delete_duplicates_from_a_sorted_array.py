# time complexity O(n)
# space complexity O(1)

# Return the number of valid entries after deletion.
def delete_duplicates(A):
    if not A:
        return 0

    write_index = 1
    for i in range(1, len(A)):
        if A[write_index - 1] != A[i]:
            A[write_index] = A[i]
            write_index += 1
    return write_index

### Test case
test_case = [
    [2,3,5,5,7,11,11,13],
    [0],
    [],
    [2,2,2,2,2,2,2,2],
]
for t in test_case:
    r = delete_duplicates(t)
    print(t, t[:r], sep=" -> ")
