

def plus_one(A):
    A[-1] += 1
    for i in reversed(range(1, len(A))):
        if A[i] != 10:
            break
        A[i] = 0
        A[i - 1] += 1
    if A[0] == 10:
        A[0] = 1
        A.append(0)

### Test case
test_case = [
    [1,2,9], # -> [1, 3, 0]
    [9,9,9], # -> [1, 0, 0, 0]
    [1,2,3], # -> [1, 2, 4]
    [0], # -> [1]
]
for t in test_case:
    plus_one(t)
    print(t)
