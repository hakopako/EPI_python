

def buy_and_sell_a_stock_twice(prices):
    max_total_profit, min_price_so_far = 0.0, float('inf')
    first_buy_sell_profits = [0] * len(prices)
    for i, price in enumerate(prices):
        min_price_so_far = min(min_price_so_far, price)
        max_total_profit = max(max_total_profit, price - min_price_so_far)
        first_buy_sell_profits[i] = max_total_profit

    max_price_so_far = float('-inf')
    for i, price in reversed(list(enumerate(prices[1:], 1))):  # TODO: what the hell is this line???
        max_price_so_far = max(max_price_so_far, price)
        max_total_profit = max(
            max_total_profit,
            max_price_so_far - price + first_buy_sell_profits[i - 1]
        )
    return max_total_profit

### Test case
test_case = [
    [12,11,13,9,12,8,14,13,15],   # -> 10
    [310,315,275,295,260,270,290,230,255,250],   # -> 55
]

for t in test_case:
    r = buy_and_sell_a_stock_twice(t)
    print(r)
