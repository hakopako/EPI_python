

def solution(A):
    result = 0
    for number in A:
        result ^= number
    return result


## Test case
test_case = [
    [9, 3, 9, 3, 9, 7, 9],   # -> 7
]

for t in test_case:
    print(solution(t))
