
"""
check if input array consists 1 to N numbers.
"""

def solution(A):
    ## solution1 t=O(2n) -> O(n) / s=O(1)
    # result = 0
    # for n in A:
    #     result ^= n
    # for i in range(1, len(A) + 1):
    #     result ^= i
    #
    # return 1 if result == 0 else 0

    ## solution2 t=O(2n||logn + n) / s=O(1)
    # A.sort()
    # for i in range(len(A)):
    #     if A[i] != i + 1:
    #         return 0
    # return 1

    ## solution3  t=O(2n) -> O(n) / s=O(n)
    return 1 if sum(A) - sum(range(1, len(A) + 1)) == 0 else 0


## Test case
test_case = [
    [4, 1, 3, 2],   # -> 1  : ture
    [4, 1, 3],      # -> 0  : false
]

for t in test_case:
    print(solution(t))
