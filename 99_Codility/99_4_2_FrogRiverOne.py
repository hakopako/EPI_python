# expected worst-case time complexity is O(N)
# expected worst-case space complexity is O(X)

def solution(X, A):
    covered, uncovered = [-1] * X, X
    for i in range(len(A)):
        if covered[A[i] - 1] != -1:
            continue
        else:
            covered[A[i] - 1] = i
            uncovered -= 1
            if uncovered == 0:
                return i
    return -1


# Test case
test_case = [
    [5, [1,3,1,4,2,3,5,4]],    # -> 6
    [4, [1,4,1,2,1,3,2,3,1]],  # -> 5
    [6, [1,3,1,4,2,3,5,4]],    # -> -1
]

for t in test_case:
    print(solution(t[0], t[1]))
