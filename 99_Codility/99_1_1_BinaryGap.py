# time complexity is O(log(N));
# space complexity is O(1).

def solution(N):
    max_gap, gap_so_far, start_gap = 0, 0, False
    while N / 2 > 0:
        if N % 2 == 1:
            if start_gap:
                max_gap = max(max_gap, gap_so_far)
                gap_so_far, start_gap = 0, True
            else:
                start_gap = True
        else:
            if start_gap:
                gap_so_far += 1
        N //= 2

    return max_gap



## Test case
test_case = [
    9,    # 1001  -> 2
    529,  # 1000010001 -> 4
    20,   # 10100 -> 1
    15,   # 1111 -> 0
    1041, # 10000010001 -> 5
]

for t in test_case:
    print(solution(t))
