

def solution(A):
    first, second = sum(A), 0  # (n)
    min_diff = float('inf')
    for n in A:
        first, second = first - n, second + n
        min_diff = min(min_diff, abs(first - second))
    return min_diff


## Test case
test_case = [
    [3, 1, 2, 4, 3],  # [3, 1, 2](6), [4, 3](7)   -> 1
]

for t in test_case:
    print(solution(t))
