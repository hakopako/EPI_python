# time complexity: O(n) -> length of the staring
# space complexity: O(c) -> number of distinct characters appering in the string. 

# using array....
def my_solution(s):
    alph = [0] * 26
    for c in s:
        alph[ord(c) - ord('a')] += 1
    return sum([n % 2 for n in alph]) <= 1

import collections
# using hash table...?
def can_form_palindrome(s):
    return sum(v % 2 for v in collections.Counter(s).values()) <= 1


## Test case

test_case = [
    'abcdedcba',   # -> True
    'aaabbbbaaa',  # -> True
    'xscxsc',   # -> True
    'xscx',   # -> False
    'aaaabc',  # -> False
    'a',  # -> True
    'ab',  # -> False
]

for t in test_case:
    #r = my_solution(t)
    r = can_form_palindrome(t)
    print(r)
