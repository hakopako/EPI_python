
import collections
def is_letter_constructible_from_magazine(letter_text, magazine_text):
    char_frequency_for_letter = collections.Counter(letter_text)

    for c in magazine_text:
        if c in char_frequency_for_letter:
            char_frequency_for_letter[c] -= 1
            if char_frequency_for_letter[c] == 0:
                del char_frequency_for_letter[c]
                if not char_frequency_for_letter:
                    return True
    return False

## Test case

test_case = [
    ['abcd', 'aabbccddssww'],   # -> True
    ['abcdz', 'aabbccddssww'],  # -> False
    ['abcdzz', 'aabbccddsswwz']  # -> False
]

for t in test_case:
    r = is_letter_constructible_from_magazine(t[0], t[1])
    print(r)
