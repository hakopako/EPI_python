
class ListNode:
    def __init__(self, data, next_node=None):
        self.data = data
        self.next = next_node

    def __repr__(self):
        return str(self.data)

    def to_array(self):
        head = self
        result = []
        while head is not None:
            result.append(head.data)
            head = head.next
        return result

def my_solution(L1, L2):
    headA, headB, l1, l2 = L1, L2, 0, 0
    while L1:
        L1, l1 = L1.next, l1 + 1
    while L2:
        L2, l2 = L2.next, l2 + 1

    if L1 is not L2:
        return None  # no conjection

    if l1 < l2:
        for _ in range(l2 - l1):
            headB = headB.next
    else:
        for _ in range(l1 - l2):
            headA = headA.next

    while headA and headB:
        if headA is headB:
            return headA
        headA, headB = headA.next, headB.next

    return None

def overlapping_no_cycle_lists(L1, L2):
    def length(L):
        length = 0
        while L:
            length += 1
            L = L.next
        return length

    L1_len, L2_len = length(L1), length(L2)
    if L1_len > L2_len:
        L1, L2 = L2, L1   # memo: L2 is the longer list
    for _ in range(L1_len - L2_len):
        L2 = L2.next

    while L1 and L2 and L1 is not L2:  # memo: while + if -> while only
        L1, L2 = L1.next, L2.next
    return L1

## Test case 1 ##############################
a, b, c, d, e, f, g, h = ListNode(1), ListNode(2), ListNode(3), ListNode(4), ListNode(5), ListNode(6), ListNode(7), ListNode(8)
con = ListNode(10)

a.next, b.next, c.next = b, c, con
d.next, e.next = e, con
con.next, f.next, g.next = f, g, h

"""
a-b-c-|
      |
 d-e-con-f-g-h
"""
print(a.to_array())
print(d.to_array())
#print(my_solution(a, d))
print(overlapping_no_cycle_lists(a, d))   # -> 10
