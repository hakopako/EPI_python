# time complexity O(n)
# space complexity O(1)

class ListNode:
    def __init__(self, data, next_node=None):
        self.data = data
        self.next = next_node

    def __repr__(self):
        return str(self.data)

    def to_array(self):
        head = self
        result = []
        while head is not None:
            result.append(head.data)
            head = head.next
        return result

def my_solution(L, k):
    result_head = head = L
    #lenght
    l = 0
    while L:
        L, l = L.next, l + 1

    # k + 1 node
    for _ in range(l - k - 1):
        head = head.next

    if k == 1:  # remove tail node
        head.next = None
    else:
        head.next = head.next.next

    return result_head


def remove_kth_last(L, k):
    dummy_head = ListNode(0, L)
    first = dummy_head.next
    for _ in range(k):
        first = first.next

    secont = dummy_head
    while first:
        first, second = first.next, second.next

    second.next = second.next.next  # mome: second points to the (k + 1) th last node. TODO: fail when k is 1.
    return dummy_head

### Test case
a, b, c, d, e, f, g, h = ListNode(1), ListNode(2), ListNode(3), ListNode(4), ListNode(5), ListNode(6), ListNode(7), ListNode(8)
a.next, b.next, c.next, d.next, e.next, f.next, g.next = b, c, d, e, f, g, h

print(a.to_array())
# my_solution(a, 3)  # delete f(6)
remove_kth_last(a, 3)
print(a.to_array())
# my_solution(a, 1)  # delete h(8)
remove_kth_last(a, 1)
print(a.to_array())
