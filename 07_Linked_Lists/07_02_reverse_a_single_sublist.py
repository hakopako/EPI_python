
class ListNode:
    def __init__(self, data=0, next_node=None):
        self.data = data
        self.next = next_node

    def to_array(self):
        node = self
        result = []
        while node is not None:
            result.append(node.data)
            node = node.next
        return result

def reverse_sublist(L, start, finish):
    dummy_head = sublist_head = ListNode(0, L)
    for _ in range(1, start):
        sublist_head = sublist_head.next

    # Reverses sublist.
    sublist_iter = sublist_head.next
    for _ in range(finish - start):
        temp = sublist_iter.next
        sublist_iter.next, temp.next, sublist_head.next = temp.next, sublist_head.next, temp  # memo: don't understand

    return dummy_head.next



### Test case
test_case = [
    [ListNode(2, ListNode(5, ListNode(7, ListNode(6, ListNode(3))))), 2, 4],   # -> [2, 6, 7, 5, 3]
]


for t in test_case:
    print(t[0].to_array())
    r = reverse_sublist(t[0], t[1], t[2])
    print(r.to_array())
