# time complexity: O(n + m)
# space complexity: O(1)

class ListNode:
    def __init__(self, data=0, next_node=None):
        self.data = data
        self.next = next_node

    def to_array(self):
        node = self
        result = []
        while node is not None:
            result.append(node.data)
            node = node.next
        return result

def my_solution(L1, L2):
    head = ListNode()
    dummy_head = head
    while L1 is not None and L2 is not None:
        if L1.data < L2.data:
            head.next = L1
            L1 = L1.next
            head = head.next
        else:
            head.next = L2
            L2 = L2.next
            head = head.next

    if L1 is not None:
        head.next = L1
    if L2 is not None:
        head.next = L2

    return dummy_head.next


def merge_two_sorted_lists(L1, L2):
    dummy_head = tail = ListNode()   # memo: init variable with one line.
    while L1 and L2:
        if L1.data < L2.data:
            tail.next, L1 = L1, L1.next   # memo: switch can work this way.
        else:
            tail.next, L2 = L2, L2.next
        tail = tail.next
    tail.next = L1 or L2   # memo: if both None, still correct as it's LinkedList.
    return dummy_head.next


### Test case
test_case = [
    [ListNode(2, ListNode(5, ListNode(7))), ListNode(3, ListNode(11))],  # -> 2, 3, 5, 7, 11
]


for t in test_case:
    print(t[0].to_array(), t[1].to_array())
    #r = my_solution(t[0], t[1])
    r = merge_two_sorted_lists(t[0], t[1])
    print(r.to_array())
