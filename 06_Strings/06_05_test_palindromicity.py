# time complexity: O(n)
# space complexity: O(1)


def is_palindrom(s):
    i, j = 0, len(s) - 1
    while i < j:
        while not s[i].isalnum() and i < j:   # memo: str.isalnum()
            i += 1
        while not s[j].isalnum() and i < j:
            j -= 1

        if s[i].lower() != s[j].lower():   # memo: str.lower()
            return False

        i, j = i + 1, j - 1
    return True

### Test case
test_case = [
    'Able was I, ere I saw Elba!',  # -> True
    ',,,,b,,,,,,',  # -> True
    ',,,b,,,x,,,',  # -> False
    '',  # -> True
    'abcba',  # -> True
    'zxdcbn',  # -> False
    'abba',  # -> True
]

for t in test_case:
    r = is_palindrom(t)
    print(r)
