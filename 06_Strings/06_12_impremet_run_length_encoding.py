
def my_encoding(s):
    if s == "":
        return ""
    result, pre, count = "", s[0], 1
    for i in range(1, len(s)):
        if pre == s[i]:
            count += 1
        else:
            result += str(count) + pre
            pre, count = s[i], 1
    result += str(count) + pre
    return result

def my_decoding(s):
    count, result = "", ""
    for c in s:
        if c.isdigit():
            count += c
        else:
            result += "".join([c]*int(count))
            count = ""
    return result

def encoding(s):
    result, count = [], 1
    for i in range(1, len(s) + 1):
        if i == len(s) or s[i] != s[i - 1]:
            result.append(str(count) + s[i - 1])
            count = 1
        else:
            count += 1
    return ''.join(result)

def decoding(s):
    count, result = 0, []
    for c in s:
        if c.isdigit():
            count = count * 10 + int(c)
        else:
            result.append(c * count)   # memo: str * int -> strstr....
            count = 0
    return ''.join(result)


### Test case
test_case_1 = [
    'aaaabcccaa',  # -> 4a1b3c2a
    'aaaaaaaaaaaaaaab',  # -> 15a1b
    '',  # -> ''
    'abcdefg',  # -> 1a1b1c1d1e1f1g
]

test_case_2 = [
    '4a1b3c2a',  # -> aaaabcccaa
    '15a1b',  # -> aaaaaaaaaaaaaaab
    '', # -> ''
    '1a1b1c1d1e1f1g',  # -> abcdefg
]

for t in test_case_1:
    #r = my_encoding(t)
    r = encoding(t)
    print(r)

for t in test_case_2:
    #r = my_decoding(t)
    r = decoding(t)
    print(r)
