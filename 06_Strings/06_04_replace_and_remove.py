

# time complexity: O(n)
# space complexity: O(n)
## this works if s doesn't have extra space.
def my_solution(size, s):
    result_arr = [0] * size * 2
    write_index = 2 * size - 1
    for i in reversed(range(size)):
        if s[i] == 'a':
            result_arr[write_index] = 'd'
            result_arr[write_index - 1] = 'd'
            write_index -= 2
        elif s[i] != 'b':
            result_arr[write_index] = s[i]
            write_index -= 1

    return [] if write_index == 2 * size - 1 else result_arr[size * 2 - write_index - 1:] or result_arr

# time complexity: O(n)
# space complexity: O(1)
## under specifit condition: s has extra space for converted string.
def replace_and_remove(size, s):
    write_idx, a_count = 0, 0
    for i in range(size):
        if s[i] != 'b':
            s[write_idx] = s[i]
            write_idx += 1
        if s[i] == 'a':
            a_count += 1

    cur_idx = write_idx - 1
    write_idx += a_count - 1
    final_size = write_idx + 1
    while cur_idx >= 0:
        if s[cur_idx] == 'a':
            s[write_idx - 1:write_idx + 1] = 'dd'
            write_idx -= 2
        else:
            s[write_idx] = s[cur_idx]
            write_idx -= 1
        cur_idx -= 1
    return final_size


### Test case
test_case = [
    ['a', 'c', 'd', 'b', 'b', 'c', 'a'],
    ['a', 'a', 'a'],
    ['b', 'b', 'b'],
    []
]

for t in test_case:
    # r = my_solution(len(t), t)
    # print(r)
    r = replace_and_remove(len(t), t)
    print(t[:r])
