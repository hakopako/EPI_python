
# time complexity: O(n)
# space complexity: O(n)
def my_solution(s):
    return " ".join(reversed(s.split(" ")))


def reverse_words(s):
    s.reverse() # TODO: error "AttributeError: 'str' object has no attribute 'reverse'". s = s[::-1] would work.

    def reverse_range(s, start, end):
        print(s[start], end)
        while start < end:
            s[start], s[end] = s[end], s[start]  # TODO: error "TypeError: 'str' object does not support item assignment"
            start, end = start + 1, end - 1

    start = 0
    while True:
        end = s.find(' ', start)   # TODO: what's this ??? probaly finding a first space of string s.
        if end < 0:
            break
        reverse_range(s, start, end - 1)
        start = end + 1
    reverse_range(s, start, len(s) - 1)


### Test case
test_case = [
    'Alice likes Bob',   # -> Bob likes Alice
    'a b c d e',   # -> e d c b a
]

for t in test_case:
    # r = my_solution(t)
    # print(r)
    reverse_words(t)
    print(t)
