
class Queue:

    def __init__(self):
        self._enq, self._deq = [], []

    def enqueue(self, x):
        self._enq.append(x)

    def dequeue(self):
        if not self._deq:
            while self._enq:
                self._deq.append(self._enq.pop())

        if not self._deq:
            print('no data')
        else:
            return self._deq.pop()


### Test case
q = Queue()

q.dequeue()  # warning
q.enqueue(3)
q.enqueue(5)
q.enqueue(4)
print(q.dequeue())  # -> 3
q.enqueue(8)
print(q.dequeue())  # -> 5
print(q.dequeue())  # -> 4
