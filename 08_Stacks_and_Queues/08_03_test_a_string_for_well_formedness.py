# time complexity: O(n)
# space complexity: O(1) ?????


def my_solution(s):
    stack = []
    for c in s:
        if stack != [] and \
           (c is ')' and stack[-1] is '(' or \
           c is ']' and stack[-1] is '[' or \
           c is '}' and stack[-1] is '{'):
           stack.pop()
        else:
           stack.append(c)

    return stack == []

def is_well_formed(s):
    left_chars, lookup = [], {'(': ')', '{': '}', '[': ']'}
    for c in s:
        if c in lookup:
            left_chars.append(c)
        elif not left_chars or lookup[left_chars.pop()] != c:
            return False
    return not left_chars   # memo: not empty?????

### Test case

test_cases = [
    '()',  # True
    '[(){}]',   # True
    '[{}{}{{{{{((()))(((()((((())))))}}}}}]',  # False
    '][()]',  # False
    '[[[[[[[[[[[[[[[[[[[[]]]]]]]]]]]]]]]]]]]]',  # True
    '[]]',   # False
]

for t in test_cases:
    #print(my_solution(t))
    print(is_well_formed(t))
