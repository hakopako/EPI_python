# time complexity: O(1)
# space complexity: O(n)


class MyElement:
    def __init__(self, data, max):
        self.data = data
        self.max = max

    def __repr__(self):
        return str(self.data) + " " + str(self.max)

class MyStack:
    def __init__(self):
        self.stack = [];

    def is_empty(self):
        return self.stack == []

    def pop(self):
        if self.is_empty():
            return None
        return self.stack.pop().data

    def push(self, data):
        elm = MyElement(data, data if self.is_empty() else max(data, self.stack[-1].max))
        self.stack.append(elm)

    def max(self):
        if self.is_empty():
            return None
        return self.stack[-1].max

import collections

class Stack:
    ElemntWithCachedMax = collections.namedtuple('ElemntWithCachedMax', ('element', 'max'))

    def __int__(self):
        self._element_with_cached_max = []

    def empty(self):
        return len(self._element_with_cached_max) == 0

    def max(self):
        if self.empty():
            raise IndexError('max(): empty stack')
        return self._element_with_cached_max[-1].max

    def pop(self):
        if self.empty():
            raise IndexError('pop(): empty stack')
        return self._element_with_cached_max[-1].element

    def push(self, x):
        self._element_with_cached_max.append(self.ElemntWithCachedMax(x, x if self.empty() else max(x, self.max())))

### Test case

#stack = MyStack()
stack = Stack()
stack.pop()  # warning
stack.push(3)
stack.push(5)
stack.push(4)
print(stack.max())  # 5
stack.push(8)
print(stack.max())  # 8
stack.pop()
print(stack.max())  # 5
