# time complexity: O(n)
# space complexity: O(1) ?????


def my_solution(RPN_expressions):
    plus = lambda x, y: x + y
    multipy = lambda x, y: x * y
    minus = lambda x, y: x - y
    divide = lambda x, y: x / y

    stack = []
    elm = RPN_expressions.split(",")
    for x in elm:
        if x is '+':
            stack.append(plus(stack.pop(), stack.pop()))
        elif x is '-':
            stack.append(minus(stack.pop(), stack.pop()))
        elif x is '*':
            stack.append(multipy(stack.pop(), stack.pop()))
        elif x is '/':
            stack.append(divide(stack.pop(), stack.pop()))
        else:
            stack.append(int(x))

    return stack.pop()

def evaluate(RPN_expressions):
    intermediate_results = []
    DELIMITER = ','
    OPERATORS = {
        '+': lambda x, y: x + y,
        '-': lambda x, y: x - y,
        '*': lambda x, y: x * y,
        '/': lambda x, y: int(x / y)
    }

    for token in RPN_expressions.split(DELIMITER):
        if token in OPERATORS:
            intermediate_results.append(OPERATORS[token](intermediate_results.pop(), intermediate_results.pop()))
        else:
            intermediate_results.append(int(token))
    return intermediate_results[-1]

### Test case

test_cases = [
    '2,3,*',   # -> 6
    '3,4,+,2,*,1,+',  # -> 15
    '1,1,+,-2,*',    # -> -4
    '-641,6,/,28,/',  # -> -2991.333333333333
]

for t in test_cases:
    print(my_solution(t))
    #print(evaluate(t))
