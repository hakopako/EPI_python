# time complexity: O(n)
# space complexity: O(n) ?????

def shortest_equivalent_path(path):
    if not path:
        raise ValueError('Empty string is not a valid path.')

    path_names = []

    # Special case:  starts with '/', which is an absolute path.
    if path[0] == '/':
        path_names.append('/')

    for token in (token for token in path.split("/") if token not in ['.', '']):
        if token == '..':
            if not path_names or path_names[-1] == '..':
                path_names.append(token)
            else:
                if path_names[-1] == '/':
                    raise ValueError('Path error')
                path_names.pop()
        else:
            path_names.append(token)

    result = '/'.join(path_names)
    return result[result.startswith('//'):]   # memo: Avoid starting '//'.

### Test case

test_cases = [
    'sc//./../tc/awk/././',   # -> tc/awk
    './aaa/bbb/.././ddd/ccc.txt',    # -> ./aaa/ddd/ccc.txt
    '/aaa/../cc/./ddd/../../././eee/ff/./ggg.tsv',    # -> /eee/ff/ggg.tsv
    './aaa/../../bb',  # -> ./../bb
    '/aa/../../cc'   # -> path error
]

for t in test_cases:
    #r = my_solution(t)
    r = shortest_equivalent_path(t)
    print(r)
