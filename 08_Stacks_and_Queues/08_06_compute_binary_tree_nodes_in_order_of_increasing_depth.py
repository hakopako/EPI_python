
class Node:
    def __init__(self, data=0, left_node=None, right_node=None):
        self.data = data
        self.left = left_node
        self.right = right_node


def my_solution(tree):
    search, depth, result, depth_result = [], 0, [], []
    search.append((tree, depth))
    while search:
        node = search.pop(0)
        if depth == node[1]:
            depth_result.append(node[0].data)
        else:
            result.append(depth_result)
            depth, depth_result = depth + 1, [node[0].data]

        if node[0].left:
            search.append((node[0].left, node[1] + 1))
        if node[0].right:
            search.append((node[0].right, node[1] + 1))

    result.append(depth_result)
    return result

def binary_tree_depth_order(tree):
    result = []
    if not tree:
        return result

    curr_depth_nodes = [tree]
    while curr_depth_nodes:
        result.append([curr.data for curr in curr_depth_nodes])
        curr_depth_nodes = [child for curr in curr_depth_nodes for child in (curr.left, curr.right) if child]
    return result

### Test case
"""
           314
         /      \
       6         6
     /   \      /   \
   271   561   2    271
  /   \     \   \     \
 28    0     3   1     28
            /  /   \
          17 401   257
               \
               641
"""

# root: depth 0
a = Node(314)
# depth 1
a.left, a.right = b, l = Node(6), Node(6)
# depth 2
b.left, b.right, l.left, l.right = c, f, j, o = Node(271), Node(561), Node(2), Node(271)
# depth 3
c.left, c.right, f.right, j.right, o.right = d, e, g, k, p = Node(28), Node(0), Node(3), Node(1), Node(28)
# depth 4
g.left, k.left, k.right = h, l, n = Node(17), Node(401), Node(257)
# depth 5
l.right = m = Node(641)

#r = my_solution(a)
r = binary_tree_depth_order(a)
print(r)  # -> [[314], [6, 6], [271, 561, 2, 271], [28, 0, 3, 1, 28], [17, 401, 257], [641]]
