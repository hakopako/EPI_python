
def matrix_search(A, x):
    row, col = 0, len(A[0]) - 1
    while row < len(A) and col >= 0:
        if A[row][col] == x:
            return True
        elif A[row][col] < x:
            row += 1
        else:
            col -= 1
    return False


## Test case

test_case = [
    [-1, 2, 4, 4, 6],
    [ 1, 5, 5, 9,21],
    [ 3, 6, 6, 9,22],
    [ 3, 6, 8,10,24],
    [ 6, 8, 9,12,25],
    [ 8,10,12,14,40],
]

print(matrix_search(test_case, 7))   # -> False
print(matrix_search(test_case, 8))   # -> True
print(matrix_search(test_case, 12))   # -> True
print(matrix_search(test_case, -1))   # -> True
print(matrix_search(test_case, 30))   # -> False
