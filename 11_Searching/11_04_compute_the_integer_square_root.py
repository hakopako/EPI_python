

def my_solution(k):
    return int(k**(1/2.0))


### Test case
test_case = [
    16,  # -> 4
    300,  # -> 17  (17**2 = 289, 18**2 = 324)
]

for t in test_case:
    r = my_solution(t)
    print(r)
