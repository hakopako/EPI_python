# time complexity: O(logn) -> Each iteration reduces the size of the candidate set by half
# space complexity: O(1)


def search_first_of_k(A, k):
    left, right, result = 0, len(A) - 1, -1
    while left <= right:
        mid = (left + right) // 2
        if A[mid] > k:
            right  = mid - 1
        elif A[mid] == k:
            result = mid
            right = mid - 1
        else:
            left = mid + 1
    return result


### Test case

test_case = [
    [[-14, -10, 2, 108, 108, 243, 285, 285, 285, 401], 108],   # -> 3
    [[-14, -10, 2, 108, 108, 243, 285, 285, 285, 401], 285],   # -> 6
    [[-14, -10, 2, 108, 108, 243, 285, 285, 285, 401], 0],   # -> -1
]

for t in test_case:
    r = search_first_of_k(t[0], t[1])
    print(r)
    
