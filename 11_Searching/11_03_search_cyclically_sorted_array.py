# time complexity: O(logn) -> Each iteration reduces the size of the candidate set by half
# space complexity: O(1)

def search_smallest(A):
    left, right = 0, len(A) - 1
    while left < right:
        mid = (left + right) // 2
        if A[mid] > A[right]:
            # Minimun must be in A[mid + 1:right + 1].
            left = mid + 1
        else:
            right = mid
    # Loop ends when left == right
    return left


### Test case

test_case = [
    [378, 478, 550, 631, 103, 203, 220, 234, 279, 368],   # -> 4
    [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,0],     # -> 18
    [100, 101, 102, 103, 104, 105, 106],  # -> 0
    [],   # error?
    [10, 11, 12, 0],   # -> 3
    [10, 11, 12, 0, 9],  # -> 3
]

for t in test_case:
    r = search_smallest(t)
    print(r)
