# time complexity: O(logn) -> Each iteration reduces the size of the candidate set by half
# space complexity: O(1)

def my_solution(A):
    left, right = 0, len(A) - 1
    while left < right:
        mid = (left + right) // 2
        if A[mid] > mid:
            right = mid - 1
        elif A[mid] == mid:
            return mid
        else:
            left = mid + 1
    return -1


def search_entry_equal_to_its_index(A):
    left, right = 0, len(A) - 1
    while left < right:
        mid = (left + right) // 2
        difference = A[mid] - mid
        if difference == 0:
            return mid
        elif difference > 0:
            right = mid - 1
        else:
            left = mid + 1
    return -1

### Test case

test_case = [
    [-2, 0, 2, 3 ,6, 7, 9],   # -> 2 or 3
    [10, 11, 12, 13, 14, 15],  # -> -1
    [-3, 1, 6, 8, 10, 17, 23, 28, 32, 33, 34, 45, 44, 46, 50],   # -> 1
]

for t in test_case:
    #r = my_solution(t)
    r = search_entry_equal_to_its_index(t)
    print(r)
